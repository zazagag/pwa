#Another isomorphic Webpack 2, React, PWA starter...

Simple application based on `Webpack 2` and `React` supported Progressive Web Application approach.

####Third-party libraries documentation

`Webpack` documentation: [webpack.github.io](https://webpack.github.io/)

`Webpack 2` documentation: [webpack.js.org](https://webpack.js.org)

`React` documentation: [facebook.github.io/react/docs/](https://facebook.github.io/react/docs/hello-world.html)

`Babel` documentation: [babeljs.io](https://babeljs.io)

####Environment

To run application in stable environment please use the Docker container that was fully described at [Docker README](./docker/prod/README.md)

####NPM main commands

|Script|Description|
|---|---|
|npm run init|Install application dependencies.|
|npm run prod|Compile and start app in `production` mode.|
|npm run dev|Compile and start app in `development` mode (HMR built-in).|

####NPM others commands

|Script|Description|
|---|---|
|npm run svg|Compile `SVG` sprite.|
|npm run hash|As we have `server/client` architecture and have separately `webpack`<br/>instances for them we will generate `hash` with own way.|
|npm run server:prod|Compile server side sources for `production` mode.|
|npm run client:prod|Compile client side sources for `production` mode.|
|npm run server:dev|Compile server side sources for `development` mode (run with `--watch`).|
|npm run client:dev|Compile client side sources for `development` mode (run with `HMR`).|
|npm run stylelint|Start `stylelint` checker.|
|npm run eslint|Start `eslint` checker.|
|npm run start:prod|Start server in `production` mode.|
|npm run start:dev|Start server in `development` mode.|
|npm run stop|Stop all `forever` instances.|
|npm run test|Work in progress...|

####License

![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-1.png)

[http://www.wtfpl.net/](http://www.wtfpl.net/) 

####Troubleshooting

- HMR on update `*.js` files triggers warn: 
```Warning: [react-router] You cannot change <Router history>; it will be ignored```

- HMR for stylesheets was implemented by using `postMessage` [styleHMR()](src/js/core/client/index.js#L40)

- In `dev` mode `HTTPS` is not working.

####Work In Progress...
