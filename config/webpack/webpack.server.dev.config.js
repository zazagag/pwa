const paths = require('./../path.config.js');
const loaders = require('./common/webpack.loaders.js');
const resolve = require('./common/webpack.resolve.js');
const getPlugins = require('./common/webpack.plugins.js');

const webpack = require('webpack');

module.exports = {
	entry: {
		server: [
			'babel-polyfill',
			paths.entry.server,
		],
	},
	target: 'node',
	output: {
		path: paths.output.server,
		filename: '[name].js',
		libraryTarget: 'commonjs2',
	},
	externals: /^[a-z\-0-9]+$/,
	plugins: getPlugins({
		webpack: webpack,
		isServer: true,
	}),
	module: {
		loaders: [...loaders, {
			test: /\.less$/,
			loader: 'ignore-loader',
		}],
	},
	resolve: resolve,
};
