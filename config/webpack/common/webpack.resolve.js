const paths = require('../../path.config.js');

module.exports = {
	modules: [
		'node_modules',
		paths.src.root,
		paths.src.public,
		paths.src.config,
	],
	extensions: ['.js', '.jsx', '.less', '.json'],
};
