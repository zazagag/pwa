const paths = require('../../path.config.js');
const WebpackCleanupPlugin = require('webpack-cleanup-plugin');
const autoprefixer = require('autoprefixer');
const OptimizeJsPlugin = require('optimize-js-plugin');
const V8LazyParseWebpackPlugin = require('v8-lazy-parse-webpack-plugin');

const getPlugins = ({webpack, isServer, isProd, ExtractTextPluginInstance} = {}) => {
	const plugins = [
		new webpack.DefinePlugin({
			isServer: JSON.stringify(!!isServer),
			isProd: JSON.stringify(!!isProd),
			'process.env': {
				NODE_ENV: JSON.stringify(isProd ? 'production' : 'development'),
			},
		}),
		new webpack.NoErrorsPlugin(),
		new WebpackCleanupPlugin({
			exclude: ['*.json', '*.svg', '.gitignore'],
		}),
	];

	if (isProd) {
		plugins.push(...[
			new webpack.optimize.UglifyJsPlugin({
				sourceMap: false,
				mangle: false,
				output: {
					comments: false,
				},
				compress: {
					warnings: false,
					conditionals: true,
					unused: true,
					comparisons: true,
					sequences: true,
					dead_code: true,
					evaluate: true,
					if_return: true,
					join_vars: true,
					negate_iife: false,
					drop_console: true,
					drop_debugger: true,
				},
			}),
			new webpack.optimize.OccurrenceOrderPlugin(),
			new OptimizeJsPlugin({
				sourceMap: false,
			}),
		]);
	}

	if (!isServer) {
		plugins.push(...[
			new webpack.ProvidePlugin({
				fetch: 'whatwg-fetch',
			}),
			new webpack.optimize.CommonsChunkPlugin({
				name: 'common',
				minChunks: 2,
			}),
		]);
	}

	if (ExtractTextPluginInstance) {
		plugins.push(...[
			new webpack.LoaderOptionsPlugin({
				debug: isProd,
				minimize: isProd,
				options: {
					lessLoader: {
						includePaths: [
							paths.src.js,
							paths.src.css,
						],
					},
					context: '/',
					postcss: [
						autoprefixer({browsers: ['last 3 versions']}),
					],
				},
			}),
			ExtractTextPluginInstance,
		]);
	}

	return plugins;
};

module.exports = getPlugins;
