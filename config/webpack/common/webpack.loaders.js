const path = require('path');

module.exports = [{
	test: /\.js|\.jsx$/,
	exclude: /node_modules/,
	loader: 'babel-loader',
}, {
	test: /\.json$/,
	loader: 'json-loader',
}, {
	test: /\.svg$/,
	loader: 'svg-inline-loader',
}];
