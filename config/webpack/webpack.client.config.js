const paths = require('./../path.config.js');
const loaders = require('./common/webpack.loaders.js');
const resolve = require('./common/webpack.resolve.js');
const getPlugins = require('./common/webpack.plugins.js');
const hash = require(paths.hash).id;
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const ExtractTextPluginInstance = new ExtractTextPlugin({
	filename: `[name].${hash}.css`,
	allChunks: true,
});

module.exports = {
	entry: {
		app: [
			'babel-polyfill',
			paths.entry.client,
		],
	},
	output: {
		path: paths.output.client,
		filename: `[name].${hash}.js`,
		publicPath: paths.public,
	},
	plugins: getPlugins({
		webpack: webpack,
		isProd: true,
		ExtractTextPluginInstance: ExtractTextPluginInstance,
	}),
	module: {
		rules: [
			...loaders,
			{
				test: /\.less$/,
				exclude: /node_modules/,
				loader: ExtractTextPluginInstance.extract({
					fallbackLoader: 'style-loader',
					loader: [
						{
							loader: 'css-loader',
						},
						{
							loader: 'postcss-loader',
						},
						{
							loader: 'less-loader',
							query: {sourceMaps: true},
						},
					],
				}),
			}],
	},
	resolve: Object.assign({}, resolve, {
		alias: {
			react: 'react-lite',
			'react-dom': 'react-lite',
		},
	}),
	node: {
		fs: 'empty',
	},
};
