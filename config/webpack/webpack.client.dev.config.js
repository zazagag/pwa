const devConfig = require('./../dev.config.js');
const paths = require('./../path.config.js');
const loaders = require('./common/webpack.loaders.js');
const resolve = require('./common/webpack.resolve.js');
const getPlugins = require('./common/webpack.plugins.js');

const webpack = require('webpack');
const HotModuleReplacementPlugin = require('webpack/lib/HotModuleReplacementPlugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const ExtractTextPluginInstance = new ExtractTextPlugin({
	filename: '[name].css',
	allChunks: true,
	sourceMap: true,
});

module.exports = {
	devtool: 'source-map',
	entry: {
		app: [
			'babel-polyfill',
			`webpack-dev-server/client?http://0.0.0.0:${devConfig.__PORT__}`,
			'react-hot-loader/patch',
			'webpack/hot/only-dev-server',
			paths.entry.client,
		],
	},
	output: {
		path: paths.output.client,
		filename: '[name].js',
		publicPath: `${devConfig.__HOST__}:${devConfig.__PORT__}/static/assets/`,
		hotUpdateChunkFilename: 'hot/hot-update.js',
		hotUpdateMainFilename: 'hot/hot-update.json',
	},
	plugins: [
		...getPlugins({
			webpack: webpack,
			ExtractTextPluginInstance: ExtractTextPluginInstance,
		}),
		new HotModuleReplacementPlugin(),
		new webpack.NamedModulesPlugin(),
	],
	module: {
		rules: [
			...loaders,
			{
				test: /\.less$/,
				exclude: /node_modules/,
				loader: ExtractTextPluginInstance.extract({
					fallbackLoader: 'style-loader',
					loader: [
						{
							loader: 'css-loader',
							query: {
								sourceMap: true,
							},
						},
						{
							loader: 'postcss-loader',
						},
						{
							loader: 'less-loader',
							query: {
								sourceMap: true,
							},
						},
					],
				}),
			}],
	},
	resolve: Object.assign({}, resolve, {
		alias: {
			react: 'react-lite',
			'react-dom': 'react-lite',
		},
	}),
	devServer: {
		contentBase: './public',
		historyApiFallback: true,
		host: '0.0.0.0',
		hot: true,
		port: devConfig.__PORT__,
		stats: {
			cached: true,
			cachedAssets: true,
			chunks: true,
			chunkModules: false,
			colors: true,
			hash: false,
			reasons: true,
			timings: true,
			version: false,
		},
		headers: {
			'Access-Control-Allow-Origin': '*',
		},
	},
	node: {
		fs: 'empty',
	},
};
