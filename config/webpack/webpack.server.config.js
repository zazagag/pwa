const paths = require('./../path.config.js');
const loaders = require('./common/webpack.loaders.js');
const resolve = require('./common/webpack.resolve.js');
const getPlugins = require('./common/webpack.plugins.js');

const hash = require(paths.hash).id;
const webpack = require('webpack');

module.exports = {
	entry: {
		server: [
			'babel-polyfill',
			paths.entry.server,
		],
	},
	target: 'node',
	output: {
		path: paths.output.server,
		filename: `[name].${hash}.js`,
		libraryTarget: 'commonjs2',
		publicPath: '../assets/',
	},
	externals: /^[a-z\-0-9]+$/,
	plugins: getPlugins({
		webpack: webpack,
		isProd: true,
		isServer: true,
	}),
	module: {
		rules: [...loaders, {
			test: /\.less$/,
			loader: 'ignore-loader',
		}],
	},
	resolve: resolve,
};
