import React, {Component, PropTypes} from 'react';
import Helmet from 'react-helmet';

import 'js/pages/Ads/AdsPage.less';

import {translate} from 'react-i18next';

@translate(['ads'])
/**
 * Ads Page.
 * @constructor
 */
class AdsPage extends Component {
	static propTypes = {
		meta: PropTypes.object,
		menu: PropTypes.object,
		t: PropTypes.func,
	};

	static defaultProps = {
		meta: {
			title: 'Ads',
			description: 'Ads',
			keywords: 'Ads',
		},
		menu: {
			path: 'О проекте — Реклама',
			title: 'Реклама',
		},
		t: () => {},
	};

	static i18nNs = ['ads'];

	/**
	 * Fetch data for page.
	 * @return {Promise}
	 */
	static fetchData = async () => {
		return new Promise((resolve) => {
			resolve({
				i18nNs: AdsPage.i18nNs,
			});
		});
	};

	render() {
		const {t} = this.props;

		return (
			<div className="screen-page screen-page--page">
				<Helmet
					title={this.props.meta.title}
					meta={[
						{name: 'description', content: this.props.meta.description},
						{name: 'keywords', content: this.props.meta.keywords},
					]}
				/>
				<div dangerouslySetInnerHTML={{__html: t('title')}} />
			</div>
		);
	}
}

export default AdsPage;
