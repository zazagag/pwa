import React, {Component, PropTypes} from 'react';

import Header from 'js/components/Header/Header';
import Menu from 'js/components/Menu/Menu';
import Footer from 'js/components/Footer/Footer';

import 'js/pages/App/AppPage.less';

class AppPage extends Component {
	static defaultProps = {
		children: null,
	};

	static propTypes = {
		children: PropTypes.object,
	};

	render() {
		return (
			<div className="page__wrap">
                <Header />
                <Menu />
				<div className="screen-page screen-page--page">
					{this.props.children}
				</div>
				<Footer />
			</div>
		);
	}
}

export default AppPage;
