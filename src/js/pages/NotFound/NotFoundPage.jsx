import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';
import Helmet from 'react-helmet';
import {translate} from 'react-i18next';

import 'js/pages/NotFound/NotFoundPage.less';

@translate(['not-found'])
/**
 * NotFound Page.
 * @constructor
 */
class NotFoundPage extends Component {
	static propTypes = {
		meta: PropTypes.object,
	};

	static defaultProps = {
		meta: {
			title: '404',
			description: '404',
			keywords: '404',
		},
	};

	static i18nNs = ['not-found'];

	/**
	 * Fetch data for page.
	 * @return {Promise}
	 */
	static fetchData = async () => {
		return new Promise((resolve) => {
			resolve({
				i18nNs: NotFoundPage.i18nNs,
			});
		});
	};

	render() {
		const {t} = this.props;

		return (
			<div className="page page--not-found">
				<Helmet
					title={this.props.meta.title}
					meta={[
						{name: 'description', content: this.props.meta.description},
						{name: 'keywords', content: this.props.meta.keywords},
					]}
				/>
				<img src="/images/404.png" />
				<div className="page__title">{t('title')}</div>
				<div className="page__text">{t('text')}</div>
				<Link to={'/'}>{t('link')}</Link>
			</div>
		);
	}
}

export default NotFoundPage;
