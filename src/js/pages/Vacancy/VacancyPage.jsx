import React, {Component, PropTypes} from 'react';
import {render} from 'react-dom';
import Helmet from 'react-helmet';

import unique from 'js/utils/unique';

import 'js/pages/Vacancy/VacancyPage.less';

import {translate} from 'react-i18next';

@translate(['vacancy'])
/**
 * Класс страницы "Вакансии".
 * @constructor VacancyPage
 * @extend Component
 */
class VacancyPage extends Component {
	static propTypes = {
		meta: PropTypes.object,
		menu: PropTypes.object,
		t: PropTypes.func,
	};

	static defaultProps = {
		meta: {
			title: 'Vacancy',
			description: 'Vacancy',
			keywords: 'Vacancy',
		},
		menu: {
			path: 'О проекте — Вакансии',
			title: 'Вакансии',
		},
		t: () => {},
	};

	static i18nNs = ['vacancy'];

	/**
	 * Fetch data for page.
	 * @return {Promise}
	 */
	static fetchData = async () => {
		return new Promise((resolve) => {
			resolve({
				i18nNs: VacancyPage.i18nNs,
			});
		});
	};

	render() {
		const {t} = this.props;

		return (
			<div className="screen-page screen-page--page">
				<Helmet
					title={this.props.meta.title}
					meta={[
						{name: 'description', content: this.props.meta.description},
						{name: 'keywords', content: this.props.meta.keywords},
					]}
				/>
				<div dangerouslySetInnerHTML={{__html: t('title')}} />
			</div>
		);
	}
}

export default VacancyPage;
