import React, {Component, PropTypes} from 'react';
import Helmet from 'react-helmet';

import 'js/pages/About/AboutPage.less';
import {translate} from 'react-i18next';

@translate(['about'])
/**
 * About page.
 * @constructor
 */
class AboutPage extends Component {
	static propTypes = {
		meta: PropTypes.object,
		menu: PropTypes.object,
		t: PropTypes.func,
	};

	static defaultProps = {
		meta: {
			title: 'About',
			description: 'About',
			keywords: 'About',
		},
		menu: {
			path: 'О проекте — О проекте',
			title: 'О проекте',
		},
		t: () => {},
	};

	static i18nNs = ['about'];

	/**
	 * Fetch data for page.
	 * @return {Promise}
	 */
	static fetchData = async () => {
		return new Promise((resolve) => {
			resolve({
				i18nNs: AboutPage.i18nNs,
			});
		});
	};

	/**
	 * @return {XML}
	 */
	render() {
		const {t} = this.props;

		return (
			<div className="screen-page screen-page--page">
				<Helmet
					title={this.props.meta.title}
					meta={[
						{name: 'description', content: this.props.meta.description},
						{name: 'keywords', content: this.props.meta.keywords},
					]}
				/>
				<div dangerouslySetInnerHTML={{__html: t('title')}} />
			</div>
		);
	}
}

export default AboutPage;
