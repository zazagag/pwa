import React, {Component, PropTypes} from 'react';
import Helmet from 'react-helmet';

import 'js/pages/Contact/ContactPage.less';

import {translate} from 'react-i18next';

@translate(['contact'])
/**
 * Contact Page.
 * @constructor
 */
class ContactPage extends Component {
	static propTypes = {
		meta: PropTypes.object,
		menu: PropTypes.object,
		t: PropTypes.func,
	};

	static defaultProps = {
		meta: {
			title: 'Contact',
			description: 'Contact',
			keywords: 'Contact',
		},
		menu: {
			path: 'О проекте — Контакты',
			title: 'Контакты',
		},
		t: () => {},
	};


	static i18nNs = ['contact'];

	/**
	 * Fetch data for page.
	 * @return {Promise}
	 */
	static fetchData = async () => {
		return new Promise((resolve) => {
			resolve({
				i18nNs: ContactPage.i18nNs,
			});
		});
	};

	render() {
		const {t} = this.props;

		return (
			<div className="screen-page screen-page--page">
				<Helmet
					title={this.props.meta.title}
					meta={[
						{name: 'description', content: this.props.meta.description},
						{name: 'keywords', content: this.props.meta.keywords},
					]}
				/>
				<div dangerouslySetInnerHTML={{__html: t('title')}} />
			</div>
		);
	}
}

export default ContactPage;
