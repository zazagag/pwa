import React, {Component, PropTypes} from 'react';
import Helmet from 'react-helmet';

import 'js/pages/Index/IndexPage.less';

import {translate} from 'react-i18next';

@translate(['index'])
/**
 * Index Page.
 * @constructor
 */
class IndexPage extends Component {
	static propTypes = {
		meta: PropTypes.object,
		t: PropTypes.func,
	};

	static defaultProps = {
		meta: {
			title: 'Main',
			description: 'Main',
			keywords: 'Main',
		},
		t: () => {},
	};

	static i18nNs = ['index'];

	/**
	 * Fetch data for page.
	 * @return {Promise}
	 */
	static fetchData = async () => {
		return new Promise((resolve) => {
			resolve({
				i18nNs: IndexPage.i18nNs,
			});
		});
	};

	render() {
		const {t} = this.props;

		return (
			<div className="page__content page__content--index">
				<Helmet
					title={this.props.meta.title}
					meta={[
						{name: 'description', content: this.props.meta.description},
						{name: 'keywords', content: this.props.meta.keywords},
					]}
				/>
				<div dangerouslySetInnerHTML={{__html: t('title')}} />
			</div>
		);
	}
}

export default IndexPage;
