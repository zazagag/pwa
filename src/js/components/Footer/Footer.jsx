import React from 'react';

import 'js/components/Footer/Footer.less';

/**
 * Footer
 * @returns {XML}
 * @constructor
 */
const Footer = () => {
    return <div className="footer">Copyright © 2016. All rights reserved.</div>;
};

export default Footer;
