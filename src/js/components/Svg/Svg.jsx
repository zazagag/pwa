import React, {Component, PropTypes} from 'react';

/**
 * Вспомогательный компонент для вставки svg.
 * @type {Component} Svg
 * @constructor
 */
class Svg extends Component {
    static defaultProps = {
        svgId: null,
        className: '',
        width: true,
        height: true,
    };

    static propTypes = {
        svgId: PropTypes.string.isRequired,
        className: PropTypes.string.isRequired,
        width: PropTypes.string.isRequired,
        height: PropTypes.string.isRequired,
        fill: PropTypes.string,
    };

    /**
     * @return {ReactElement} markup
     */
    render() {
        return <svg
                className={this.props.className}
                width={this.props.width}
                height={this.props.height}
                dangerouslySetInnerHTML={{__html: `<use xlink:href="#${this.props.svgId}" id=${this.props.svgId}/>`}}
            />;
    }
}

export default Svg;
