import React from 'react';

import Svg from 'js/components/Svg/Svg';

import 'js/components/Header/Header.less';

/**
 * Header
 * @returns {XML}
 * @constructor
 */
const Header = () => {
    return (
        <div className="header">
            <div className="header__logo">
                <Svg
                    svgId="shopping-cart"
                    className="modal__icon--close"
                    width="80px"
                    height="80px"
                />
            </div>
            <div className="header__name">
                BasketLand<sup>®</sup>
            </div>
        </div>
    );
};

export default Header;
