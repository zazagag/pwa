import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';
import {translate} from 'react-i18next';
import Cookies from 'js-cookie';
import i18n from 'i18next';

import 'js/components/Menu/Menu.less';

@translate(['menu'])
/**
 * Menu
 */
class Menu extends Component {
	static propTypes = {
		t: PropTypes.func,
	};

	static defaultProps = {
		t: () => {},
	};

	/**
	 * @param {object} props props
	 * @constructor
	 */
	constructor(props) {
		super(props);

		this.onChange = this.onChange.bind(this);
	}

	/**
	 * Callback on change language
	 * @param {React.Event} e event
	 * @return {void}
	 */
	onChange(e) {
		const {target} = e;
		const locale = target.value;
		i18n.changeLanguage(locale);
		Cookies.set('lang', locale);
	}

	/**
	 * @return {XML}
	 */
	render() {
		const {t} = this.props;
		const locale = i18n.language;

		console.log('----------------------');
		console.log(locale);
		console.log('----------------------');
		return <div className="menu">
			<div className="menu__item lang">
				<select onChange={this.onChange} value={locale}>
					<option name="ru" value="ru">{t('lang_ru')}</option>
					<option name="en" value="en">{t('lang_en')}</option>
				</select>
			</div>
			<div className="menu__item">
				<Link to="/">
					{t('main')}
				</Link>
			</div>
			<div className="menu__item">
				<Link to="/about">
					{t('about')}
				</Link>
			</div>
			<div className="menu__item">
				<Link to="/contact">
					{t('contact')}
				</Link>
			</div>
			<div className="menu__item">
				<Link to="/ads">
					{t('ads')}
				</Link>
			</div>
			<div className="menu__item">
				<Link to="/vacancy">
					{t('vacancy')}
				</Link>
			</div>
		</div>;
	}
}

export default Menu;
