/**
 * All stylesheets links.
 * @type {NodeList}
 */
const links = document.querySelectorAll('link[href][rel=stylesheet]');

/**
 * Style HMR. Update stylesheets on HMR re-compile source postMessage.
 * @param {object} event postMessage event data
 * @returns {boolean}
 */
const styleHMR = (event) => {
	if (typeof event.data !== 'string') {
		return false;
	}

	if (event.data.indexOf('webpackHotUpdate') !== 0) {
		return false;
	}

	links.forEach((link) => {
		link.href = link.href.replace(/(\?\d+)?$/, `?${Date.now()}`);
		console.log(`[HMR] Was updated those links: ${link.href}`);
	});

	return true;
};

export default styleHMR;
