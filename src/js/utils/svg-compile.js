const paths = require('./../../../config/path.config.js');

const SVGSprite = require('svg-sprite');
const path = require('path');
const fs = require('fs');
const File = require('vinyl');
const glob = require('glob');

const cwd = path.resolve(paths.svg.items);
const sprite = new SVGSprite({
	dest: './public/static/svg',
	mode: {
		symbol: true,
	},
	shape: {
		transform: {
			svgo: {
				plugins: [
					{transformsWithOnePath: true},
					{moveGroupAttrsToElems: false},
				],
			},
		},
	},
	svg: {
		rootAttributes: {
			style: 'width: 0; height: 0; position: absolute; left: -1000px;'
		},
	}
});

glob('**/*.svg', {cwd: cwd}, (err, files) => {
	files.forEach((file) => {
		sprite.add(new File({
			path: path.join(cwd, file),
			base: cwd,
			contents: fs.readFileSync(path.join(cwd, file)),
		}));
	});

	sprite.compile((error, result, data) => {
		for (const type in result.symbol) {
			fs.writeFileSync(paths.svg.sprite, result.symbol[type].contents);
		}
	});
});
