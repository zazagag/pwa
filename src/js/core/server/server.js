import React from 'react';
import {match, RouterContext} from 'react-router';
import {renderToString} from 'react-dom/server';
import Helmet from 'react-helmet';
import {I18nextProvider} from 'react-i18next';
import i18n from 'i18next';

import api from 'api.js';
import routers from 'js/core/routers/routers';

const devConfig = require('dev.config.js');
const hash = require('../../../../assets/hash').id;

const routes = routers();

/**
 * Return target page constructor.
 * @param {object} props props
 * @return {Promise}
 */
const getTargetPage = async (props) => {
	return new Promise((resolve) => {
		const route = props.routes[props.routes.length - 1];
		route.getComponents(null, (location, page) => {
			resolve(page);
		});
	});
};

/**
 * Enter point of application.
 * @param {object} req object
 * @param {object} res object
 * @param {function} next function
 * @return {Promise}
 */
const page = (req, res, next) => {
	return new Promise(async (resolve, reject) => {
		/**
		 * Matches a set of routes to a location, without rendering.
		 */
		match({routes, location: req.url}, (err, redirectLocation, renderProps) => {
			i18n.changeLanguage(req.language);
			const windowI18n = {
				locale: i18n.language,
				ns: i18n.options.ns,
			};

			if (err) {
				console.log(err);
				reject({
					html: `Error!\r\n[${err.name}]: ${err.message}\r\n${err.stack}`,
					status: 500,
				});
			}

			try {
				const componentHTML = renderToString(<I18nextProvider i18n={i18n}>
					<RouterContext {...renderProps} />
				</I18nextProvider>);
				let cssAppLink;
				let jsCommonLink;
				let jsAppLink;

				if (isProd) {
					cssAppLink = `/static/assets/app.${hash}.css`;
					jsCommonLink = `/static/assets/common.${hash}.js`;
					jsAppLink = `/static/assets/app.${hash}.js`;
				} else {
					cssAppLink = `${devConfig.__HOST__}:${devConfig.__PORT__}/static/assets/app.css`;
					jsCommonLink = `${devConfig.__HOST__}:${devConfig.__PORT__}/static/assets/common.js`;
					jsAppLink = `${devConfig.__HOST__}:${devConfig.__PORT__}/static/assets/app.js`;
				}

//				jsCommonLink = '';
//				jsAppLink = '';

				const head = Helmet.rewind();
				const html = `<!DOCTYPE html>
					<html>
						<head>
							<meta charset="utf-8">
							<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
							<meta name="viewport" content="width=1024">
							<link rel="stylesheet" href="${cssAppLink}"/>
							${head.title}
							${head.meta}
							<script charSet="UTF-8">window.__i18n=${JSON.stringify(windowI18n)}</script>
						</head>
						<body>
							${require('../../../../assets/icon-sprite.svg')}
							<div class="root" id="root">${componentHTML}</div>
							<script type="application/javascript" src="${jsCommonLink}"></script>
							<script type="application/javascript" src="${jsAppLink}"></script>
							<div class="modal_node" id="modalNode"></div>
						</body>
					</html>`;

				const {status} = renderProps.routes[renderProps.routes.length - 1];
				resolve({html: html, status: status});
			} catch (e) {
				console.log(e);
				reject({
					html: `Error!\r\n[${e.name}]: ${e.message}\r\n${e.stack}`,
					status: 500,
				});
			}
		});
	});
};

module.exports = {
	api: api,
	page: page,
};
