import express from 'express';
import bodyParser from 'body-parser';
import Mailer from '../components/mail/Mailer';

const router = express.Router();

router.use(bodyParser.json());

router.post('/vacancy', (req, res) => {
	try {
		const Mail = new Mailer();
		Mail.sendVacancy(req.body).then((data) => {
			res.json(data);
		}).catch((err) => {
			res.json(err);
		});
	} catch (err) {
		res.json({
			success: false,
			error: err,
		});
	}
});

router.post('/contact', (req, res) => {
	try {
		const Mail = new Mailer();
		Mail.sendContact(req.body).then((data) => {
			res.json(data);
		}).catch((err) => {
			res.json(err);
		});
	} catch (err) {
		res.json({
			success: false,
			error: err,
		});
	}
});

router.all('*', (req, res) => {
	res.json({error: 'unknown service!'});
});

export default router;
