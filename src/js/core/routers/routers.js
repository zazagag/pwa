import React from 'react';
import {Router, Route, IndexRoute} from 'react-router';
import i18n from 'i18next';

import AppPage from 'js/pages/App/AppPage';
import NotFoundPage from 'js/pages/NotFound/NotFoundPage';
import unique from 'js/utils/unique';

/**
 * Prepares pages data
 * @param {function} page page
 * @return {void}
 */
const fetchPageData = async (page) => {
	const pageData = await page.fetchData();

	return new Promise((resolve) => {
		i18n.loadNamespaces(pageData.i18nNs, () => {
			resolve();
		});
	});
};

/**
 * @return {XML[]}
 */
const routers = () => {
	return <Router>
			<Route path="/" component={AppPage}>
				<IndexRoute
					status={200}
					getComponents={(location, callback) => {
						System.import('js/pages/Index/IndexPage').then(async (IndexPage) => {
							await fetchPageData(IndexPage.default);
							callback(null, IndexPage.default);
						});
					}}
				/>
				<Route
					status={200}
					path="/about"
					getComponents={(location, callback) => {
						System.import('js/pages/About/AboutPage').then(async (AboutPage) => {
							await fetchPageData(AboutPage.default);
							callback(null, AboutPage.default);
						});
					}}
				/>
				<Route
					status={200}
					path="/vacancy"
					getComponents={(location, callback) => {
						System.import('js/pages/Vacancy/VacancyPage').then(async (VacancyPage) => {
							await fetchPageData(VacancyPage.default);
							callback(null, VacancyPage.default);
						});
					}}
				/>
				<Route
					status={200}
					path="/contact"
					getComponents={(location, callback) => {
						System.import('js/pages/Contact/ContactPage').then(async (ContactPage) => {
							await fetchPageData(ContactPage.default);
							callback(null, ContactPage.default);
						});
					}}
				/>
				<Route
					status={200}
					path="/ads"
					getComponents={(location, callback) => {
						System.import('js/pages/Ads/AdsPage').then(async (AdsPage) => {
							await fetchPageData(AdsPage.default);
							callback(null, AdsPage.default);
						});
					}}
				/>
			</Route>
			<Route status={404} path="*" component={NotFoundPage} />
		</Router>;
};

export default routers;
