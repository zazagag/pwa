import React from 'react';
import {render} from 'react-dom';
import {Router, browserHistory, match} from 'react-router';
import {AppContainer} from 'react-hot-loader';
import {I18nextProvider} from 'react-i18next';
import i18n from 'i18next';
import XHR from 'i18next-xhr-backend';

import routers from 'js/core/routers/routers';
import styleHMR from 'js/utils/styleHMR';

import 'less/index.less';

window.addEventListener('unhandledrejection', (e) => {
	// NOTE: e.preventDefault() must be manually called to prevent the default
	// action which is currently to log the stack trace to console.warn
	e.preventDefault();
	// NOTE: parameters are properties of the event detail property
	const reason = e.detail.reason;
	const promise = e.detail.promise;
	console.log(reason);
	console.log(promise);
	// See Promise.onPossiblyUnhandledRejection for parameter documentation
});

const root = document.querySelector('#root');

const initI18n = async () => {
	return new Promise((resolve, reject) => {
		i18n
			.use(XHR)
			.init({
				lng: window.__i18n.locale,
				fallbackLng: 'ru',
				ns: window.__i18n.ns,
				defaultNS: 'menu',
				interpolation: {
					escapeValue: false,
				},
				load: 'current',
				backend: {
					loadPath: '/locales/{{lng}}/{{ns}}.json',
					jsonIndent: 1,
				},
				debug: true,
			}, (err) => {
				if (err) {
					reject(err);
					return;
				}

				resolve();
			});
	});
};

/**
 * Update stylesheets.
 */
addEventListener('message', styleHMR);

match({history: browserHistory, routes: routers()}, async (error, redirectLocation, renderProps) => {
	await initI18n();

	render(
			<AppContainer>
				<I18nextProvider i18n={i18n}>
					<Router history={browserHistory} {...renderProps} />
				</I18nextProvider>
			</AppContainer>, root);

	/**
	 * @todo checking out warning [HMR] unexpected require from disposed module.
	 */
	if (module.hot) {
		module.hot.accept();
	}
});
