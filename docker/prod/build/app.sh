#!/bin/sh

set -e

cd /var/www/master

git config --global user.email "zazagag@gmail.com"
git config --global user.name "Andrei Nikitin"

npm run init

npm run prod:server && npm run prod:client

chown www-data:www-data -R /var/www

