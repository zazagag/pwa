#!/bin/sh

set -e

if [ -e /.my-initialized ]; then
    exit 0
fi

echo "USER=${DOCKER_USER}" >> /var/www/.profile

echo "export APPLICATION_ROOT=/var/www/master/" >> /var/www/.profile

mkdir -p /var/www/.ssh
mkdir -p /root/.ssh

echo "${DOCKER_SSH_PUBKEY}" >> /var/www/.ssh/authorized_keys
echo "${DOCKER_SSH_PUBKEY}" >> /root/.ssh/authorized_keys

chown www-data:www-data -R /var/www/.ssh
chmod go-rwx -R /var/www/.ssh
chmod go-rwx -R /root/.ssh

touch /.my-initialized
