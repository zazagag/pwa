#!/bin/sh

set -e

cd /var/www/master

git config --global user.email "zazagag@gmail.com"
git config --global user.name "Andrei Nikitin"

npm run init

chown www-data:www-data -R /var/www

