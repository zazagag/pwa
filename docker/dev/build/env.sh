#!/bin/sh

set -e

#Generate SSL
#sudo openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048

#openssl req -nodes -newkey rsa:2048 -keyout /etc/nginx/ssl/pwa.key -out /etc/nginx/ssl/pwa.csr

#openssl x509 -req -days 365 -in /etc/nginx/ssl/pwa.csr -signkey /etc/nginx/ssl/pwa.key -out /etc/nginx/ssl/certificate.crt

#Add SSL certificates.
sudo mkdir /etc/nginx/ssl
mv /build/conf/ssl/certificate.crt /etc/nginx/ssl/certificate.crt
mv /build/conf/ssl/pwa.csr /etc/nginx/ssl/pwa.csr
mv /build/conf/ssl/pwa.key /etc/nginx/ssl/pwa.key
mv /build/conf/ssl/dhparam.pem /etc/ssl/certs/dhparam.pem

mv /build/conf/nginx.conf /etc/nginx/sites-available/default

mv /build/conf/my-init.sh /etc/my_init.d/my-init.sh

