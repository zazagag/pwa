#Environment for Another isomorphic Webpack 2, React, PWA starter...

Based on `zazagag/base` image. For further documentation please [check it out.](https://github.com/zazagag/docker/blob/master/base/README.md)

####Inside
* ubuntu 16.04
* node 6.x
* nginx (SSL, http2)
* ssh
* mc
* git
* net-tools
* realpath
* sudo

####Workflow
- run `build` Docker command.
- run `run` Docker command.
- login into container via `ssh`.
- run `npm run dev`
- run `npm run dev:start`
- open Docker host url.
- enjoy!

####Running container ports explanations
|Description|Outer Port|Internal Port|
|---|---|---|
|Standard *HTTP* port|80|80|
|Standard *HTTPS* port|443|443|
|Standard *SSH* port|2222|22|
|Port for *Webpack DevServer*|4444|4444|

####Docker commands.
|Description||
|---|---|
|Build from Dockerfile.|docker build -f ./docker/dev/Dockerfile -t *[image_name]* .|
|Run container.|docker run -d -p *[ip]*:80:80 -p *[ip]*:443:443 -p *[ip]*:2222:22 -p *[ip]*:4444:24444<br> -e DOCKER_SSH_PUBKEY="$(cat ~/.ssh/id_rsa.pub)" --name *[container_name]* *[image_name]*|

####Free SSL
Built in for localhost by [Create Self-Signed SSL Certificate Nginx tutorial](http://www.queryadmin.com/858/create-self-signed-ssl-certificate-nginx/).

For public domain use [Digital Ocean tutorial](https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-16-04).

####SSH
SSH server run on `2222` port.

`ssh -p 2222 root@<ip>`

`ssh -p 2222 www-data@<ip>`

####Nginx
Nginx gets config by including `/var/www/master/config/nginx*.conf;`.
So any changes in that config should be updated by restarting Nginx.

`sudo sv restart nginx` - Restart Nginx.

####my-init.sh
All specified user options could be stored in `pwa/build/conf/my-init.sh`

####Tag & Push
```
docker images
docker tag x zazagag/pwa:0.1
docker login hub.docker.com
docker push zazagag/pwa:0.1
docker push zazagag/pwa
```
