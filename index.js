const express = require('express');
const requestLanguage = require('express-request-language');
const cookieParser = require('cookie-parser');
const i18next = require('i18next');
const Backend = require('i18next-sync-fs-backend');
const hash = require('./assets/hash.json').id;

const src = process.env.NODE_ENV === 'production' ? `./assets/server.${hash}` : './assets/server';
const page = require(src).page;
const api = require(src).api;

const app = express();

const port = 3333;

app.use(cookieParser());
app.use(requestLanguage({
	languages: ['ru', 'en'],
	cookie: {
		name: 'lang',
	},
}));

i18next
	.use(Backend)
	.init({
		lngs: ['ru', 'en'],
		fallbackLng: 'ru',
		ns: ['menu'],
		interpolation: {
			escapeValue: false,
		},
		load: 'all',
		detection: {
			order: ['cookie', 'header'],
			lookupCookie: 'lang',
		},
		backend: {
			loadPath: '../public/locales/{{lng}}/{{ns}}.json',
			jsonIndent: 4,
		},
	}, () => {});

/**
 * Catch API requests.
 */
app.use('/api', api);

process.on('unhandledRejection', (reason, promise) => {
	console.log('---[unhandledRejection]---');
	console.log(reason);
	console.log(promise);
});

/**
 * Catch common requests.
 */
app.use((req, res, next) => {
	page(req, res, next).then((options) => {
		res
			.status(options.status)
			.header({'Content-Type': 'text/html'})
			.end(options.html);
	}).catch((err) => {
		res
			.status(err.status)
			.end(err.html);
	});
});

/**
 * Start HTTP server.
 * @type {*|{remove}}
 */
app.listen(port, () => {
	console.log('Listening on port %d', port);
});
